package in.glivade.vaccine.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import in.glivade.vaccine.AppointmentActivity;
import in.glivade.vaccine.R;
import in.glivade.vaccine.app.MyPreference;

public class FcmNotification extends FirebaseMessagingService {

    private static final String NOTIFY_ID = "vaccine";
    private static final String NOTIFY_CHANNEL = "Vaccine";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        MyPreference preference = new MyPreference(this);
        if (preference.getId() == -1) return;
        JSONObject object = new JSONObject(remoteMessage.getData());
        String title = object.optString("title");
        String description = object.optString("description");
        Intent intent = new Intent(this, AppointmentActivity.class);
        PendingIntent pendingIntentContent = PendingIntent.getActivity(this,
                4, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFY_ID);
        builder.setColor(ContextCompat.getColor(this, R.color.primary))
                .setTicker("Appointment alert")
                .setContentTitle(title)
                .setContentText(description)
                .setContentIntent(pendingIntentContent)
                .setSmallIcon(R.drawable.ic_notify)
                .setLargeIcon(bitmapLargeIcon)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(description));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFY_ID, NOTIFY_CHANNEL,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify((int) (Math.random() * 5000 + 1), builder.build());
        }
    }
}
