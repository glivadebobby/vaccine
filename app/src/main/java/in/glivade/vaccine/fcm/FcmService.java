package in.glivade.vaccine.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import in.glivade.vaccine.app.MyPreference;

public class FcmService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        storeToken(token);
    }

    private void storeToken(String token) {
        MyPreference preference = new MyPreference(this);
        preference.setFcmToken(token);
        preference.setTokenUploaded(false);
    }
}
