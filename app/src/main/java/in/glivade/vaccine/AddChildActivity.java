package in.glivade.vaccine;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.app.MyPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.vaccine.app.Api.KEY_DOB;
import static in.glivade.vaccine.app.Api.KEY_ERROR;
import static in.glivade.vaccine.app.Api.KEY_GENDER;
import static in.glivade.vaccine.app.Api.KEY_MESSAGE;
import static in.glivade.vaccine.app.Api.KEY_NAME;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.KEY_USER_ID;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_ADD_CHILD;
import static in.glivade.vaccine.util.Parser.getDate;

public class AddChildActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutName;
    private TextInputEditText editTextName, editTextDob;
    private RadioGroup groupGender;
    private Button buttonSubmit;
    private AnimationDrawable animationDrawable;
    private MyPreference preference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);
        initObjects();
        initCallbacks();
        initToolbar();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            animationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == editTextDob) {
            promptDatePickerDialog();
        } else if (view == buttonSubmit) {
            processAddChild();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutName = findViewById(R.id.name);
        editTextName = findViewById(R.id.input_name);
        editTextDob = findViewById(R.id.input_dob);
        groupGender = findViewById(R.id.grp_gender);
        buttonSubmit = findViewById(R.id.btn_submit);

        context = this;
        animationDrawable = (AnimationDrawable) findViewById(R.id.add_child).getBackground();
        preference = new MyPreference(context);
        progressDialog = new ProgressDialog(context);
    }

    private void initCallbacks() {
        editTextDob.setOnClickListener(this);
        buttonSubmit.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void promptDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                editTextDob.setText(getDate(year, month, dayOfMonth));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void processAddChild() {
        String name = editTextName.getText().toString().trim();
        String gender = "Male";
        if (groupGender.getCheckedRadioButtonId() == R.id.radio_female) gender = "Female";
        String dob = editTextDob.getText().toString().trim();
        if (name.isEmpty()) {
            layoutName.setError(getString(R.string.error_empty));
        } else if (dob.isEmpty()) {
            Toast.makeText(this, "Please provide DOB", Toast.LENGTH_SHORT).show();
        } else addChild(name, gender, dob);
    }

    private void addChild(final String name, final String gender, final String dob) {
        showProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleAddChildResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(AddChildActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_ADD_CHILD);
                params.put(KEY_USER_ID, String.valueOf(preference.getId()));
                params.put(KEY_NAME, name);
                params.put(KEY_GENDER, gender);
                params.put(KEY_DOB, dob);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "add-child");
    }

    private void handleAddChildResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

            if (error == 0) {
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Adding child..");
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
