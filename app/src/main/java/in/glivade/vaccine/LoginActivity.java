package in.glivade.vaccine;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.app.MyPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.vaccine.app.Api.KEY_ERROR;
import static in.glivade.vaccine.app.Api.KEY_ID;
import static in.glivade.vaccine.app.Api.KEY_MESSAGE;
import static in.glivade.vaccine.app.Api.KEY_MOBILE;
import static in.glivade.vaccine.app.Api.KEY_NAME;
import static in.glivade.vaccine.app.Api.KEY_PASS;
import static in.glivade.vaccine.app.Api.KEY_ROLE;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_LOGIN;
import static in.glivade.vaccine.app.Api.VALUE_USER;
import static in.glivade.vaccine.app.MyActivity.launch;
import static in.glivade.vaccine.app.MyActivity.launchClearStack;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private EditText editTextMobile, editTextPass;
    private Button buttonLogin;
    private TextView textViewNewUser;
    private AnimationDrawable animationDrawable;
    private MyPreference preference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            animationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonLogin) {
            processLogin();
        } else if (view == textViewNewUser) {
            launch(context, RegisterActivity.class);
        }
    }

    private void initObjects() {
        editTextMobile = findViewById(R.id.input_mobile);
        editTextPass = findViewById(R.id.input_pass);
        buttonLogin = findViewById(R.id.btn_login);
        textViewNewUser = findViewById(R.id.txt_new_user);

        context = this;
        animationDrawable = (AnimationDrawable) findViewById(R.id.login).getBackground();
        preference = new MyPreference(context);
        progressDialog = new ProgressDialog(context);
    }

    private void initCallbacks() {
        buttonLogin.setOnClickListener(this);
        textViewNewUser.setOnClickListener(this);
    }

    private void processLogin() {
        String mobile = editTextMobile.getText().toString().trim();
        String pass = editTextPass.getText().toString().trim();
        if (validateInput(mobile, pass)) {
            showProgressDialog();
            loginUser(mobile, pass);
        }
    }

    private boolean validateInput(String mobile, String pass) {
        if (TextUtils.isEmpty(mobile)) {
            editTextMobile.requestFocus();
            editTextMobile.setError(getString(R.string.error_empty));
            return false;
        } else if (mobile.length() < 10) {
            editTextMobile.requestFocus();
            editTextMobile.setError(String.format(Locale.getDefault(),
                    getString(R.string.error_length), "Mobile No", 10, "digits"));
            return false;
        } else if (pass.isEmpty()) {
            editTextPass.requestFocus();
            editTextPass.setError(getString(R.string.error_empty));
            return false;
        } else if (pass.length() < 6) {
            editTextPass.requestFocus();
            editTextPass.setError(getString(R.string.error_pass_length));
            return false;
        }
        return true;
    }

    private void loginUser(final String mobile, final String pass) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleLoginResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_LOGIN);
                params.put(KEY_MOBILE, mobile);
                params.put(KEY_PASS, pass);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "login");
    }

    private void handleLoginResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);

            if (error == 0) {
                if (jsonObject.getString(KEY_ROLE).equalsIgnoreCase(VALUE_USER)) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    preference.setId(jsonObject.getInt(KEY_ID));
                    preference.setName(jsonObject.getString(KEY_NAME));
                    launchClearStack(context, MainActivity.class);
                } else {
                    Toast.makeText(context, "Unauthorized access", Toast.LENGTH_SHORT).show();
                }
            } else Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Logging in..");
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
