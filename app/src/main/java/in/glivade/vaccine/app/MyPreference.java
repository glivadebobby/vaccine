package in.glivade.vaccine.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class MyPreference {

    private static final String PREF_USER = "user";
    private static final String PREF_LOCATION = "location";
    private static final String PREF_ALARM = "alarm";
    private static final String PREF_MISC = "misc";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String HOUR = "hour";
    private static final String MINUTE = "minute";
    private static final String SCHEDULED = "scheduled";
    private static final String FCM_TOKEN = "fcm_token";
    private static final String TOKEN_UPLOADED = "token_uploaded";
    private SharedPreferences preferencesUser, preferencesLocation, preferencesAlarm, preferencesMisc;

    public MyPreference(Context context) {
        preferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        preferencesLocation = context.getSharedPreferences(PREF_LOCATION, Context.MODE_PRIVATE);
        preferencesAlarm = context.getSharedPreferences(PREF_ALARM, Context.MODE_PRIVATE);
        preferencesMisc = context.getSharedPreferences(PREF_MISC, Context.MODE_PRIVATE);
    }

    public int getId() {
        return preferencesUser.getInt(encode(ID), -1);
    }

    public void setId(int id) {
        preferencesUser.edit().putInt(encode(ID), id).apply();
    }

    public String getName() {
        return preferencesUser.getString(encode(NAME), null);
    }

    public void setName(String name) {
        preferencesUser.edit().putString(encode(NAME), name).apply();
    }

    public double getLatitude() {
        String lat = preferencesLocation.getString(encode(LATITUDE), null);
        return lat == null ? 12.9716 : Double.parseDouble(lat);
    }

    public void setLatitude(double latitude) {
        preferencesLocation.edit().putString(encode(LATITUDE), String.valueOf(latitude)).apply();
    }

    public double getLongitude() {
        String lng = preferencesLocation.getString(encode(LONGITUDE), null);
        return lng == null ? 77.5946 : Double.parseDouble(lng);
    }

    public void setLongitude(double longitude) {
        preferencesLocation.edit().putString(encode(LONGITUDE), String.valueOf(longitude)).apply();
    }

    public int getHour() {
        return preferencesAlarm.getInt(encode(HOUR), 8);
    }

    public void setHour(int hour) {
        preferencesAlarm.edit().putInt(encode(HOUR), hour).apply();
    }

    public int getMinute() {
        return preferencesAlarm.getInt(encode(MINUTE), 30);
    }

    public void setMinute(int minute) {
        preferencesAlarm.edit().putInt(encode(MINUTE), minute).apply();
    }

    public boolean isScheduled() {
        return preferencesAlarm.getBoolean(encode(SCHEDULED), false);
    }

    public void setScheduled(boolean scheduled) {
        preferencesAlarm.edit().putBoolean(encode(SCHEDULED), scheduled).apply();
    }

    public boolean isTokenUploaded() {
        return preferencesMisc.getBoolean(encode(TOKEN_UPLOADED), false);
    }

    public void setTokenUploaded(boolean tokenUploaded) {
        preferencesMisc.edit().putBoolean(encode(TOKEN_UPLOADED), tokenUploaded).apply();
    }

    public String getFcmToken() {
        return preferencesMisc.getString(encode(FCM_TOKEN), null);
    }

    public void setFcmToken(String fcmToken) {
        preferencesMisc.edit().putString(encode(FCM_TOKEN), fcmToken).apply();
    }

    public void clearUser() {
        setTokenUploaded(false);
        preferencesUser.edit().clear().apply();
        preferencesAlarm.edit().clear().apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
