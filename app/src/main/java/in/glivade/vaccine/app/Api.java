package in.glivade.vaccine.app;

public class Api {
    /**
     * Api Keys
     */
    public static final String KEY_TAG = "tag";
    public static final String KEY_ID = "id";
    public static final String KEY_ROLE = "role";
    public static final String KEY_NAME = "name";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_PASS = "password";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_ERROR = "error";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_PREVENTS = "prevents";
    public static final String KEY_DOSE1 = "dose_1";
    public static final String KEY_DOSE2 = "dose_2";
    public static final String KEY_DOSE3 = "dose_3";
    public static final String KEY_DOSE4 = "dose_4";
    public static final String KEY_DOSE5 = "dose_5";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_DOB = "dob";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_CHILD_ID = "child_id";
    public static final String KEY_CHILD_NAME = "child_name";
    public static final String KEY_CENTER = "center";
    public static final String KEY_REVIEWS = "reviews";
    public static final String KEY_LAST_UPDATED = "last_updated";
    public static final String KEY_APPOINTMENT_AT = "appointment_at";
    public static final String KEY_REQUESTED_AT = "requested_at";
    public static final String KEY_STATUS = "status";
    public static final String KEY_CENTER_ID = "center_id";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_DISTANCE = "distance";
    public static final String KEY_USER_NAME = "user_name";
    public static final String KEY_USER_MOBILE = "user_mobile";
    public static final String KEY_RATING = "rating";
    public static final String KEY_REVIEW = "review";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESC = "description";
    public static final String KEY_CREATED_AT = "created_at";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_FCM_TOKEN = "fcm_token";
    /**
     * Api Values
     */
    public static final String VALUE_LOGIN = "login";
    public static final String VALUE_REGISTER = "register";
    public static final String VALUE_USER = "user";
    public static final String VALUE_UPDATE_FCM = "update_fcm";
    public static final String VALUE_FEEDS = "feeds";
    public static final String VALUE_VACCINES = "vaccines";
    public static final String VALUE_CHILDREN = "children";
    public static final String VALUE_DELETE_CHILD = "delete_child";
    public static final String VALUE_ADD_CHILD = "add_child";
    public static final String VALUE_MY_CENTERS = "my_centers";
    public static final String VALUE_ADD_REVIEW = "add_review";
    public static final String VALUE_EDIT_REVIEW = "edit_review";
    public static final String VALUE_REVIEWS = "reviews";
    public static final String VALUE_DOCTORS = "doctors";
    public static final String VALUE_REQUEST_APPOINTMENT = "request_appointment";
    public static final String VALUE_MY_APPOINTMENTS = "my_appointments";
    public static final String VALUE_CANCEL_APPOINTMENT = "cancel_appointment";
    /**
     * Api URLS
     */
    public static final String BASE_URL = "http://vaccine.glivade.in/";
    public static final String URL = BASE_URL + "api.php";
}
