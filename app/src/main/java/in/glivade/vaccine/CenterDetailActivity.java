package in.glivade.vaccine;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.glivade.vaccine.adapter.DoctorAdapter;
import in.glivade.vaccine.adapter.ReviewAdapter;
import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.app.MyPreference;
import in.glivade.vaccine.model.Center;
import in.glivade.vaccine.model.Doctor;
import in.glivade.vaccine.model.Review;
import in.glivade.vaccine.util.Parser;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.vaccine.app.Api.KEY_APPOINTMENT_AT;
import static in.glivade.vaccine.app.Api.KEY_CENTER;
import static in.glivade.vaccine.app.Api.KEY_CENTER_ID;
import static in.glivade.vaccine.app.Api.KEY_CHILD_ID;
import static in.glivade.vaccine.app.Api.KEY_ERROR;
import static in.glivade.vaccine.app.Api.KEY_ID;
import static in.glivade.vaccine.app.Api.KEY_LAST_UPDATED;
import static in.glivade.vaccine.app.Api.KEY_MESSAGE;
import static in.glivade.vaccine.app.Api.KEY_MOBILE;
import static in.glivade.vaccine.app.Api.KEY_NAME;
import static in.glivade.vaccine.app.Api.KEY_PHONE;
import static in.glivade.vaccine.app.Api.KEY_RATING;
import static in.glivade.vaccine.app.Api.KEY_REVIEW;
import static in.glivade.vaccine.app.Api.KEY_REVIEWS;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.KEY_USER_ID;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_DOCTORS;
import static in.glivade.vaccine.app.Api.VALUE_REQUEST_APPOINTMENT;
import static in.glivade.vaccine.app.Api.VALUE_REVIEWS;
import static in.glivade.vaccine.app.MyActivity.launch;
import static in.glivade.vaccine.app.MyActivity.launchNavigation;
import static in.glivade.vaccine.app.MyActivity.launchWithBundle;

public class CenterDetailActivity extends AppCompatActivity implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener, ReviewAdapter.ReviewCallback {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout layoutCenterData;
    private TextView textViewName, textViewAddress, textViewContactPerson, textViewContactMobile,
            textViewNoDoctor, textViewNoReview;
    private RatingBar ratingBar;
    private ImageView imageViewLocation;
    private RecyclerView viewDoctors, viewReviews;
    private Button buttonRequestAppointment, buttonWriteReview;
    private AnimationDrawable mAnimationDrawable;
    private List<Doctor> doctorList;
    private List<Review> reviewList;
    private DoctorAdapter doctorAdapter;
    private ReviewAdapter reviewAdapter;
    private MyPreference preference;
    private ProgressDialog progressDialog;
    private int childId, centerId;
    private double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_center_detail);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
        refreshLayout.setRefreshing(true);
        getDoctors();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewLocation) {
            launchNavigation(context, latitude, longitude);
        } else if (view == buttonRequestAppointment) {
            promptDatePickerDialog();
        } else if (view == buttonWriteReview) {
            Bundle bundle = new Bundle();
            bundle.putInt(KEY_ID, centerId);
            launchWithBundle(this, WriteReviewActivity.class, bundle);
        }
    }

    @Override
    public void onRefresh() {
        getDoctors();
    }

    @Override
    public void onEditClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_REVIEW, reviewList.get(position));
        launchWithBundle(this, EditReviewActivity.class, bundle);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        layoutCenterData = findViewById(R.id.center_data);
        textViewName = findViewById(R.id.txt_name);
        textViewAddress = findViewById(R.id.txt_address);
        textViewContactPerson = findViewById(R.id.txt_contact_person);
        textViewContactMobile = findViewById(R.id.txt_contact_mobile);
        textViewNoDoctor = findViewById(R.id.txt_no_doctor);
        textViewNoReview = findViewById(R.id.txt_no_review);
        ratingBar = findViewById(R.id.rating);
        imageViewLocation = findViewById(R.id.img_location);
        viewDoctors = findViewById(R.id.doctors);
        viewReviews = findViewById(R.id.reviews);
        buttonRequestAppointment = findViewById(R.id.btn_request_appointment);
        buttonWriteReview = findViewById(R.id.btn_write_review);

        context = this;
        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.center_detail).getBackground();
        doctorList = new ArrayList<>();
        reviewList = new ArrayList<>();
        preference = new MyPreference(context);
        progressDialog = new ProgressDialog(context);
        doctorAdapter = new DoctorAdapter(doctorList);
        reviewAdapter = new ReviewAdapter(reviewList, this, preference.getId());
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        imageViewLocation.setOnClickListener(this);
        buttonRequestAppointment.setOnClickListener(this);
        buttonWriteReview.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            childId = bundle.getInt(KEY_CHILD_ID);
            Center center = bundle.getParcelable(KEY_CENTER);
            if (center != null) {
                centerId = center.getId();
                latitude = center.getLatitude();
                longitude = center.getLongitude();
                textViewName.setText(center.getName());
                textViewAddress.setText(center.getAddress());
                textViewContactPerson.setText(center.getUserName());
                textViewContactMobile.setText(center.getUserMobile());
                ratingBar.setRating((float) center.getRating());
            }
        }
    }

    private void initRecyclerView() {
        viewDoctors.setLayoutManager(new LinearLayoutManager(this));
        viewDoctors.setAdapter(doctorAdapter);
        viewDoctors.setNestedScrollingEnabled(false);

        viewReviews.setLayoutManager(new LinearLayoutManager(this));
        viewReviews.setAdapter(reviewAdapter);
        viewReviews.setNestedScrollingEnabled(false);
    }

    private void initRefresh() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void getDoctors() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleDoctorResult(response);
                        getReviews();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                Toast.makeText(CenterDetailActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_DOCTORS);
                params.put(KEY_CENTER_ID, String.valueOf(centerId));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "doctors");
    }

    private void handleDoctorResult(String response) {
        try {
            doctorList.clear();
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                doctorList.add(new Doctor(object.getString(KEY_NAME), object.getString(KEY_PHONE)));
            }
            doctorAdapter.notifyDataSetChanged();
            textViewNoDoctor.setVisibility(doctorList.isEmpty() ? View.VISIBLE : View.GONE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getReviews() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        refreshLayout.setRefreshing(false);
                        handleReviewResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                Toast.makeText(CenterDetailActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_REVIEWS);
                params.put(KEY_CENTER_ID, String.valueOf(centerId));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "reviews");
    }

    private void handleReviewResult(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            reviewList.clear();
            layoutCenterData.setVisibility(View.VISIBLE);
            boolean hasReviewed = false;
            JSONArray reviewArray = jsonObject.getJSONArray(KEY_REVIEWS);
            for (int i = 0; i < reviewArray.length(); i++) {
                JSONObject object = reviewArray.getJSONObject(i);
                int userId = object.getInt(KEY_USER_ID);
                if (!hasReviewed) hasReviewed = preference.getId() == userId;
                reviewList.add(new Review(object.getInt(KEY_ID), object.getInt(KEY_CENTER_ID),
                        userId, Float.parseFloat(object.getString(KEY_RATING)),
                        object.getString(KEY_REVIEW), object.getString(KEY_NAME),
                        object.getString(KEY_MOBILE),
                        Parser.getRelativeTime(object.getString(KEY_LAST_UPDATED))));
            }
            reviewAdapter.notifyDataSetChanged();
            textViewNoReview.setVisibility(reviewList.isEmpty() ? View.VISIBLE : View.GONE);
            buttonWriteReview.setVisibility(hasReviewed ? View.GONE : View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void promptDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        final int hour = calendar.get(Calendar.HOUR);
        final int minute = calendar.get(Calendar.MINUTE);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                promptTimePickerDialog(year, month, dayOfMonth, hour, minute);
            }
        }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void promptTimePickerDialog(final int year, final int month, final int dayOfMonth, int hour, int minute) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                requestAppointment(Parser.getTime(year, month, dayOfMonth, hourOfDay, minute));
            }
        }, hour, minute, false);
        timePickerDialog.show();
    }

    private void requestAppointment(final String appointmentTime) {
        showProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleRequestAppointmentResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(CenterDetailActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_REQUEST_APPOINTMENT);
                params.put(KEY_USER_ID, String.valueOf(preference.getId()));
                params.put(KEY_CHILD_ID, String.valueOf(childId));
                params.put(KEY_CENTER_ID, String.valueOf(centerId));
                params.put(KEY_APPOINTMENT_AT, appointmentTime);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "request_appointment");
    }

    private void handleRequestAppointmentResult(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            if (error == 0) {
                launch(this, AppointmentActivity.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Requesting appointment..");
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
