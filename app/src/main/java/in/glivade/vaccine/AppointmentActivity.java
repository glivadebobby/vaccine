package in.glivade.vaccine;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.glivade.vaccine.adapter.AppointmentAdapter;
import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.app.MyPreference;
import in.glivade.vaccine.db.AppDatabase;
import in.glivade.vaccine.model.Appointment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.vaccine.app.Api.KEY_ADDRESS;
import static in.glivade.vaccine.app.Api.KEY_APPOINTMENT_AT;
import static in.glivade.vaccine.app.Api.KEY_CENTER_ID;
import static in.glivade.vaccine.app.Api.KEY_CHILD_ID;
import static in.glivade.vaccine.app.Api.KEY_CHILD_NAME;
import static in.glivade.vaccine.app.Api.KEY_ERROR;
import static in.glivade.vaccine.app.Api.KEY_ID;
import static in.glivade.vaccine.app.Api.KEY_LATITUDE;
import static in.glivade.vaccine.app.Api.KEY_LONGITUDE;
import static in.glivade.vaccine.app.Api.KEY_MESSAGE;
import static in.glivade.vaccine.app.Api.KEY_NAME;
import static in.glivade.vaccine.app.Api.KEY_REQUESTED_AT;
import static in.glivade.vaccine.app.Api.KEY_STATUS;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.KEY_USER_ID;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_CANCEL_APPOINTMENT;
import static in.glivade.vaccine.app.Api.VALUE_MY_APPOINTMENTS;
import static in.glivade.vaccine.app.MyActivity.launchNavigation;
import static in.glivade.vaccine.util.Parser.getNiceTime;

public class AppointmentActivity extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener, AppointmentAdapter.AppointmentCallback {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewAppointments;
    private List<Appointment> appointmentList;
    private AppointmentAdapter appointmentAdapter;
    private AnimationDrawable animationDrawable;
    private MyPreference preference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        initObjects();
        initToolbar();
        initRecyclerView();
        initRefresh();
        loadAppointment();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            animationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onLocationClick(int position) {
        Appointment appointment = appointmentList.get(position);
        launchNavigation(context, appointment.getLatitude(), appointment.getLongitude());
    }

    @Override
    public void onDeleteClick(int position) {
        promptDeleteDialog(position);
    }

    @Override
    public void onRefresh() {
        getAppointments();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewAppointments = findViewById(R.id.appointments);

        context = this;
        appointmentList = new ArrayList<>();
        appointmentAdapter = new AppointmentAdapter(appointmentList, this);
        animationDrawable = (AnimationDrawable) findViewById(R.id.appointment).getBackground();
        preference = new MyPreference(context);
        progressDialog = new ProgressDialog(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewAppointments.setLayoutManager(new LinearLayoutManager(context));
        viewAppointments.setAdapter(appointmentAdapter);
    }

    private void initRefresh() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void loadAppointment() {
        LiveData<List<Appointment>> data = AppDatabase.getAppDatabase(context).appointmentDao().getAll();
        data.observe(this, new Observer<List<Appointment>>() {
            @Override
            public void onChanged(@Nullable List<Appointment> appointments) {
                if (appointments == null || appointments.isEmpty()) {
                    refreshLayout.setRefreshing(true);
                    getAppointments();
                } else {
                    appointmentList.clear();
                    appointmentList.addAll(appointments);
                    appointmentAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void getAppointments() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        refreshLayout.setRefreshing(false);
                        handleAppointmentResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                Toast.makeText(AppointmentActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_MY_APPOINTMENTS);
                params.put(KEY_USER_ID, String.valueOf(preference.getId()));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "appointments");
    }

    private void handleAppointmentResult(String response) {
        try {
            List<Appointment> appointments = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                int userId = jsonObject.getInt(KEY_USER_ID);
                int childId = jsonObject.getInt(KEY_CHILD_ID);
                int centerId = jsonObject.getInt(KEY_CENTER_ID);
                double latitude = jsonObject.getDouble(KEY_LATITUDE);
                double longitude = jsonObject.getDouble(KEY_LONGITUDE);
                String name = jsonObject.getString(KEY_NAME);
                String childName = jsonObject.getString(KEY_CHILD_NAME);
                String address = jsonObject.getString(KEY_ADDRESS);
                String appointmentAt = getNiceTime(jsonObject.getString(KEY_APPOINTMENT_AT));
                String requestedAt = getNiceTime(jsonObject.getString(KEY_REQUESTED_AT));
                String status = jsonObject.getString(KEY_STATUS);
                appointments.add(new Appointment(id, userId, childId, centerId, latitude,
                        longitude, name, childName, address, appointmentAt, requestedAt, status));
            }
            new AddAppointmentTask(AppDatabase.getAppDatabase(this))
                    .execute(appointments.toArray(new Appointment[appointments.size()]));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void promptDeleteDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Cancel");
        builder.setMessage("Are you sure you want to cancel the appointment?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteAppointment(position);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void deleteAppointment(final int position) {
        showProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleDeleteAppointmentResponse(position, response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(AppointmentActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_CANCEL_APPOINTMENT);
                params.put(KEY_ID, String.valueOf(appointmentList.get(position).getId()));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "delete-appointment");
    }

    private void handleDeleteAppointmentResponse(int position, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

            if (error == 0) {
                new DeleteAppointmentTask(AppDatabase.getAppDatabase(this))
                        .execute(appointmentList.get(position));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Cancelling appointment..");
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private static class AddAppointmentTask extends AsyncTask<Appointment, Void, Void> {

        private AppDatabase database;

        AddAppointmentTask(AppDatabase database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Appointment... appointments) {
            database.appointmentDao().nuke();
            database.appointmentDao().insertAll(appointments);
            return null;
        }
    }

    private static class DeleteAppointmentTask extends AsyncTask<Appointment, Void, Void> {

        private AppDatabase database;

        DeleteAppointmentTask(AppDatabase database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Appointment... appointment) {
            database.appointmentDao().deleteAll(appointment);
            return null;
        }
    }
}
