package in.glivade.vaccine;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.app.MyPreference;
import in.glivade.vaccine.model.Center;
import in.glivade.vaccine.util.CenterInfoWindow;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.vaccine.app.Api.KEY_ADDRESS;
import static in.glivade.vaccine.app.Api.KEY_CENTER;
import static in.glivade.vaccine.app.Api.KEY_CHILD_ID;
import static in.glivade.vaccine.app.Api.KEY_DISTANCE;
import static in.glivade.vaccine.app.Api.KEY_ID;
import static in.glivade.vaccine.app.Api.KEY_LATITUDE;
import static in.glivade.vaccine.app.Api.KEY_LONGITUDE;
import static in.glivade.vaccine.app.Api.KEY_NAME;
import static in.glivade.vaccine.app.Api.KEY_RATING;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.KEY_USER_ID;
import static in.glivade.vaccine.app.Api.KEY_USER_MOBILE;
import static in.glivade.vaccine.app.Api.KEY_USER_NAME;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_MY_CENTERS;
import static in.glivade.vaccine.app.MyActivity.launchWithBundle;

public class CenterActivity extends AppCompatActivity implements OnMapReadyCallback {

    private Context context;
    private Toolbar toolbar;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private List<Center> centerList;
    private AnimationDrawable animationDrawable;
    private MyPreference preference;
    private int childId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_center);
        initObjects();
        initToolbar();
        processBundle();
        initMap();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            animationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(preference.getLatitude(),
                preference.getLongitude()), 8));
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Bundle bundle = new Bundle();
                bundle.putInt(KEY_CHILD_ID, childId);
                bundle.putParcelable(KEY_CENTER, (Center) marker.getTag());
                launchWithBundle(CenterActivity.this, CenterDetailActivity.class, bundle);
            }
        });
        getCenters();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        context = this;
        centerList = new ArrayList<>();
        animationDrawable = (AnimationDrawable) findViewById(R.id.center).getBackground();
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            childId = bundle.getInt(KEY_CHILD_ID);
        }
    }

    private void initMap() {
        mapFragment.getMapAsync(this);
    }

    private void getCenters() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleCenterResult(response);
                        createMarkers();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CenterActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_MY_CENTERS);
                params.put(KEY_CHILD_ID, String.valueOf(childId));
                params.put(KEY_LATITUDE, String.valueOf(preference.getLatitude()));
                params.put(KEY_LONGITUDE, String.valueOf(preference.getLongitude()));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "centers");
    }

    private void handleCenterResult(String response) {
        try {
            centerList.clear();
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                int userId = jsonObject.getInt(KEY_USER_ID);
                double latitude = jsonObject.getDouble(KEY_LATITUDE);
                double longitude = jsonObject.getDouble(KEY_LONGITUDE);
                double rating = 0;
                if (!jsonObject.isNull(KEY_RATING)) rating = jsonObject.getDouble(KEY_RATING);
                double distance = jsonObject.getDouble(KEY_DISTANCE);
                String name = jsonObject.getString(KEY_NAME);
                String address = jsonObject.getString(KEY_ADDRESS);
                String userName = jsonObject.getString(KEY_USER_NAME);
                String userMobile = jsonObject.getString(KEY_USER_MOBILE);
                centerList.add(new Center(id, userId, latitude, longitude, rating, distance, name,
                        address, userName, userMobile));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (centerList.isEmpty())
            Toast.makeText(this, "No centers found", Toast.LENGTH_SHORT).show();
    }

    private void createMarkers() {
        map.clear();
        map.setInfoWindowAdapter(new CenterInfoWindow(this));
        for (Center center : centerList) {
            MarkerOptions options = new MarkerOptions()
                    .position(new LatLng(center.getLatitude(), center.getLongitude()));
            map.addMarker(options)
                    .setTag(center);
        }
    }
}
