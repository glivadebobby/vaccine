package in.glivade.vaccine;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import in.glivade.vaccine.app.MyPreference;

import static in.glivade.vaccine.app.MyActivity.launch;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MyPreference preference = new MyPreference(SplashActivity.this);
                if (preference.getId() == -1) launch(SplashActivity.this, LoginActivity.class);
                else launch(SplashActivity.this, MainActivity.class);
            }
        }, 1000);
    }
}
