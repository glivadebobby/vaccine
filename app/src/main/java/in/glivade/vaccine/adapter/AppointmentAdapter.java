package in.glivade.vaccine.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.glivade.vaccine.R;
import in.glivade.vaccine.model.Appointment;

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.AppointmentHolder> {

    private List<Appointment> appointmentList;
    private AppointmentCallback callback;

    public AppointmentAdapter(List<Appointment> appointmentList, AppointmentCallback callback) {
        this.appointmentList = appointmentList;
        this.callback = callback;
    }

    @NonNull
    @Override
    public AppointmentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AppointmentHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_appointment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AppointmentHolder holder, int position) {
        Appointment appointment = appointmentList.get(position);
        holder.textViewCenter.setText(appointment.getName());
        holder.textViewName.setText(appointment.getChildName());
        holder.textViewRequestedAt.setText(appointment.getRequestedAt());
        holder.textViewAppointmentAt.setText(appointment.getAppointmentAt());
        holder.textViewStatus.setText(appointment.getStatus());
    }

    @Override
    public int getItemCount() {
        return appointmentList.size();
    }

    public interface AppointmentCallback {
        void onLocationClick(int position);

        void onDeleteClick(int position);
    }

    class AppointmentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewCenter, textViewName, textViewRequestedAt, textViewAppointmentAt,
                textViewStatus;
        ImageView imageViewLocation, imageViewDelete;

        AppointmentHolder(View itemView) {
            super(itemView);
            textViewCenter = itemView.findViewById(R.id.txt_center);
            textViewName = itemView.findViewById(R.id.txt_name);
            textViewRequestedAt = itemView.findViewById(R.id.txt_requested_at);
            textViewAppointmentAt = itemView.findViewById(R.id.txt_appointment_at);
            textViewStatus = itemView.findViewById(R.id.txt_status);
            imageViewLocation = itemView.findViewById(R.id.img_location);
            imageViewDelete = itemView.findViewById(R.id.img_delete);
            imageViewLocation.setOnClickListener(this);
            imageViewDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == imageViewLocation) callback.onLocationClick(getLayoutPosition());
            else if (v == imageViewDelete) callback.onDeleteClick(getLayoutPosition());
        }
    }
}
