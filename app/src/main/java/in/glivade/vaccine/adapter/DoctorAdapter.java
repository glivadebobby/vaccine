package in.glivade.vaccine.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.glivade.vaccine.R;
import in.glivade.vaccine.model.Doctor;

public class DoctorAdapter extends RecyclerView.Adapter<DoctorAdapter.DoctorHolder> {

    private List<Doctor> doctorList;

    public DoctorAdapter(List<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

    @NonNull
    @Override
    public DoctorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DoctorHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_doctor, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorHolder holder, int position) {
        Doctor doctor = doctorList.get(position);
        holder.textViewName.setText(doctor.getName());
        holder.textViewContactMobile.setText(doctor.getPhone());
    }

    @Override
    public int getItemCount() {
        return doctorList.size();
    }

    class DoctorHolder extends RecyclerView.ViewHolder {

        TextView textViewName, textViewContactMobile;

        DoctorHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.txt_name);
            textViewContactMobile = itemView.findViewById(R.id.txt_contact_mobile);
        }
    }
}
