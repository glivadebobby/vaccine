package in.glivade.vaccine.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.glivade.vaccine.R;
import in.glivade.vaccine.model.Children;

public class ChildrenAdapter extends RecyclerView.Adapter<ChildrenAdapter.ChildrenHolder> {

    private List<Children> childrenList;
    private ChildrenCallback callback;

    public ChildrenAdapter(List<Children> childrenList, ChildrenCallback callback) {
        this.childrenList = childrenList;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ChildrenHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChildrenHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_child, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChildrenHolder holder, int position) {
        Children children = childrenList.get(position);
        holder.textViewName.setText(children.getName());
        holder.textViewGender.setText(children.getGender());
        holder.textViewDob.setText(children.getDob());
    }

    @Override
    public int getItemCount() {
        return childrenList.size();
    }

    public interface ChildrenCallback {
        void onDeleteClick(int position);

        void onGetAppointmentClick(int position);
    }

    class ChildrenHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewName, textViewGender, textViewDob;
        ImageView imageViewDelete;
        Button buttonGetAppointment;

        ChildrenHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.txt_name);
            textViewGender = itemView.findViewById(R.id.txt_gender);
            textViewDob = itemView.findViewById(R.id.txt_dob);
            imageViewDelete = itemView.findViewById(R.id.img_delete);
            buttonGetAppointment = itemView.findViewById(R.id.btn_get_appointment);
            imageViewDelete.setOnClickListener(this);
            buttonGetAppointment.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == imageViewDelete) callback.onDeleteClick(getLayoutPosition());
            else if (v == buttonGetAppointment)
                callback.onGetAppointmentClick(getLayoutPosition());
        }
    }
}
