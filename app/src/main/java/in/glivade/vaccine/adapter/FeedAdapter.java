package in.glivade.vaccine.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.glivade.vaccine.R;
import in.glivade.vaccine.app.GlideApp;
import in.glivade.vaccine.model.Feed;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedHolder> {

    private List<Feed> feedList;

    public FeedAdapter(List<Feed> feedList) {
        this.feedList = feedList;
    }

    @NonNull
    @Override
    public FeedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FeedHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_feed, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FeedHolder holder, int position) {
        Feed feed = feedList.get(position);
        holder.textViewTitle.setText(feed.getTitle());
        holder.textViewDesc.setText(feed.getDesc());
        holder.textViewDate.setText(feed.getDate());
        if (feed.getImage().isEmpty() || feed.getImage().equalsIgnoreCase("null"))
            holder.imageViewFeed.setVisibility(View.GONE);
        else holder.imageViewFeed.setVisibility(View.VISIBLE);
        GlideApp.with(holder.itemView)
                .load(feed.getFeedImage())
                .into(holder.imageViewFeed);
    }

    @Override
    public int getItemCount() {
        return feedList.size();
    }

    class FeedHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewDesc, textViewDate;
        ImageView imageViewFeed;

        FeedHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.txt_title);
            textViewDesc = itemView.findViewById(R.id.txt_desc);
            textViewDate = itemView.findViewById(R.id.txt_date);
            imageViewFeed = itemView.findViewById(R.id.img_feed);
        }
    }
}
