package in.glivade.vaccine.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.glivade.vaccine.R;
import in.glivade.vaccine.model.Vaccine;

public class VaccineAdapter extends RecyclerView.Adapter<VaccineAdapter.VaccineHolder> {

    private List<Vaccine> vaccineList;

    public VaccineAdapter(List<Vaccine> vaccineList) {
        this.vaccineList = vaccineList;
    }

    @NonNull
    @Override
    public VaccineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VaccineHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vaccine, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VaccineHolder holder, int position) {
        Vaccine vaccine = vaccineList.get(position);
        holder.textViewName.setText(vaccine.getName());
        holder.textViewPrevents.setText(vaccine.getPrevents());
        holder.textViewDose1.setText(getDosage(vaccine.getDose1()));
        holder.textViewDose2.setText(getDosage(vaccine.getDose2()));
        holder.textViewDose3.setText(getDosage(vaccine.getDose3()));
        holder.textViewDose4.setText(getDosage(vaccine.getDose4()));
        holder.textViewDose5.setText(getDosage(vaccine.getDose5()));
    }

    @Override
    public int getItemCount() {
        return vaccineList.size();
    }

    private String getDosage(int week) {
        if (week == -1) return "-";
        else if (week == 0) return "Birth";
        else {
            String dosage = "";
            int year = week / 48;
            if (year == 1) dosage += "1 year";
            else if (year > 1) dosage += year + " years";
            int month = (week % 48) / 4;
            if (month == 1) dosage += " 1 month";
            else if (month > 1) dosage += " " + month + " months";
            week = week % 4;
            if (week == 1) dosage += " 1 week";
            else if (week > 1) dosage += " " + week + " weeks";
            return dosage.trim();
        }
    }

    class VaccineHolder extends RecyclerView.ViewHolder {

        TextView textViewName, textViewPrevents, textViewDose1, textViewDose2, textViewDose3,
                textViewDose4, textViewDose5;

        VaccineHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.txt_name);
            textViewPrevents = itemView.findViewById(R.id.txt_prevents);
            textViewDose1 = itemView.findViewById(R.id.txt_dose1);
            textViewDose2 = itemView.findViewById(R.id.txt_dose2);
            textViewDose3 = itemView.findViewById(R.id.txt_dose3);
            textViewDose4 = itemView.findViewById(R.id.txt_dose4);
            textViewDose5 = itemView.findViewById(R.id.txt_dose5);
        }
    }
}
