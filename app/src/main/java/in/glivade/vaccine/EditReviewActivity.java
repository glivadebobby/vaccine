package in.glivade.vaccine;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.model.Review;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.vaccine.app.Api.KEY_ERROR;
import static in.glivade.vaccine.app.Api.KEY_ID;
import static in.glivade.vaccine.app.Api.KEY_MESSAGE;
import static in.glivade.vaccine.app.Api.KEY_RATING;
import static in.glivade.vaccine.app.Api.KEY_REVIEW;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_EDIT_REVIEW;

public class EditReviewActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private RatingBar ratingBar;
    private TextInputLayout layoutReview;
    private TextInputEditText editTextReview;
    private Button buttonUpdate;
    private AnimationDrawable mAnimationDrawable;
    private ProgressDialog progressDialog;
    private int reviewId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_review);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonUpdate) {
            processEditReview();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        ratingBar = findViewById(R.id.rating);
        layoutReview = findViewById(R.id.review);
        editTextReview = findViewById(R.id.input_review);
        buttonUpdate = findViewById(R.id.btn_update);

        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.edit_review).getBackground();
        progressDialog = new ProgressDialog(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        buttonUpdate.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Review review = bundle.getParcelable(KEY_REVIEW);
            if (review != null) {
                reviewId = review.getId();
                ratingBar.setRating(review.getRating());
                editTextReview.setText(review.getReview());
            }
        }
    }

    private void processEditReview() {
        float rating = ratingBar.getRating();
        String review = editTextReview.getText().toString().trim();
        if (rating < 1) {
            Toast.makeText(this, "Invalid rating", Toast.LENGTH_SHORT).show();
        } else if (review.isEmpty()) {
            layoutReview.setError(getString(R.string.error_empty));
        } else {
            editReview(rating, review);
        }
    }

    private void editReview(final float rating, final String review) {
        showProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleWriteReviewResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(EditReviewActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_EDIT_REVIEW);
                params.put(KEY_ID, String.valueOf(reviewId));
                params.put(KEY_RATING, String.valueOf(rating));
                params.put(KEY_REVIEW, review);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "edit-review");
    }

    private void handleWriteReviewResult(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            if (error == 0) finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Updating review..");
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
