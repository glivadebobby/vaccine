package in.glivade.vaccine.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import in.glivade.vaccine.R;
import in.glivade.vaccine.model.Center;

public class CenterInfoWindow implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public CenterInfoWindow(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View markerView = LayoutInflater.from(context).inflate(R.layout.item_center, null);
        TextView textViewName, textViewAddress, textViewContactPerson, textViewContactMobile;
        RatingBar ratingBar;
        textViewName = markerView.findViewById(R.id.txt_name);
        textViewAddress = markerView.findViewById(R.id.txt_address);
        textViewContactPerson = markerView.findViewById(R.id.txt_contact_person);
        textViewContactMobile = markerView.findViewById(R.id.txt_contact_mobile);
        ratingBar = markerView.findViewById(R.id.rating);

        Center center = (Center) marker.getTag();
        if (center != null) {
            textViewName.setText(center.getName());
            textViewAddress.setText(center.getAddress());
            textViewContactPerson.setText(center.getUserName());
            textViewContactMobile.setText(center.getUserMobile());
            ratingBar.setRating((float) center.getRating());
        }
        return markerView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
