package in.glivade.vaccine.util;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Parser {
    public static String getNiceTime(String dateTime) {
        try {
            Date parsedTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.getDefault()).parse(dateTime);
            return new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(parsedTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    public static String getRelativeTime(String dateTime) {
        try {
            Date parsedTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.getDefault()).parse(dateTime);
            return DateUtils.getRelativeTimeSpanString(parsedTime.getTime(), System.currentTimeMillis(),
                    DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    public static String getDate(int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public static String getTime(int year, int month, int dayOfMonth, int hourOfDay, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth, hourOfDay, minute);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }
}
