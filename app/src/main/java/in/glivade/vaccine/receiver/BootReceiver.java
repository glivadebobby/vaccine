package in.glivade.vaccine.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import in.glivade.vaccine.util.NotificationHelper;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null &&
                intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            NotificationHelper.scheduleRepeatingNotification(context);
        }
    }
}
