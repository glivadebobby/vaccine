package in.glivade.vaccine.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.glivade.vaccine.MainActivity;
import in.glivade.vaccine.R;
import in.glivade.vaccine.model.Appointment;
import in.glivade.vaccine.model.Children;
import in.glivade.vaccine.model.Vaccine;

import static in.glivade.vaccine.db.AppDatabase.getAppDatabase;

public class AlarmReceiver extends BroadcastReceiver {

    private static final String NOTIFY_ID = "vaccine";
    private static final String NOTIFY_CHANNEL = "Vaccine";

    @Override
    public void onReceive(Context context, Intent intent) {
        List<Vaccine> vaccines = new ArrayList<>(getAppDatabase(context).vaccineDao().get());
        List<Children> children = new ArrayList<>(getAppDatabase(context).childrenDao().get());
        List<Appointment> appointments = new ArrayList<>(getAppDatabase(context).appointmentDao().get());
        for (Children child : children) {
            int weeks = 0;
            try {
                Date parsedTime = new SimpleDateFormat("dd MMM yyyy",
                        Locale.getDefault()).parse(child.getDob());
                weeks = (int) ((System.currentTimeMillis() - parsedTime.getTime()) / (7 * 24 * 60 * 60 * 1000));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (weeks == 0) continue;
            for (Vaccine vaccine : vaccines) {
                if (weeks == vaccine.getDose1()) {
                    sendVaccinationNotification(context, getDescription(1, child.getName(), vaccine.getName()));
                } else if (weeks == vaccine.getDose2()) {
                    sendVaccinationNotification(context, getDescription(2, child.getName(), vaccine.getName()));
                } else if (weeks == vaccine.getDose3()) {
                    sendVaccinationNotification(context, getDescription(3, child.getName(), vaccine.getName()));
                } else if (weeks == vaccine.getDose4()) {
                    sendVaccinationNotification(context, getDescription(4, child.getName(), vaccine.getName()));
                } else if (weeks == vaccine.getDose5()) {
                    sendVaccinationNotification(context, getDescription(5, child.getName(), vaccine.getName()));
                }
            }
        }
        for (Appointment appointment : appointments) {
            try {
                if (DateUtils.isToday(new SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
                        .parse(appointment.getAppointmentAt()).getTime())) {
                    sendAppointmentNotification(context, "You have an appointment for your child "
                            + appointment.getChildName() + " in " + appointment.getName() + " at "
                            + appointment.getAppointmentAt() + ". Ignore if already visited doctor.");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private String getDescription(int dosage, String name, String vaccine) {
        return "Your child " + name + " has to be vaccinated" + " (Dosage " + dosage + ") with " +
                vaccine + ". Ignore if already done.";
    }

    private void sendVaccinationNotification(Context context, String description) {
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntentContent = PendingIntent.getActivity(context,
                4, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFY_ID);
        builder.setColor(ContextCompat.getColor(context, R.color.primary))
                .setTicker("Vaccination alert")
                .setContentTitle("Vaccination alert")
                .setContentText(description)
                .setContentIntent(pendingIntentContent)
                .setSmallIcon(R.drawable.ic_notify)
                .setLargeIcon(bitmapLargeIcon)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(description));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFY_ID, NOTIFY_CHANNEL,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify((int) (Math.random() * 5000 + 1), builder.build());
        }
    }

    private void sendAppointmentNotification(Context context, String description) {
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntentContent = PendingIntent.getActivity(context,
                4, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFY_ID);
        builder.setColor(ContextCompat.getColor(context, R.color.primary))
                .setTicker("Appointment alert")
                .setContentTitle("Appointment alert")
                .setContentText(description)
                .setContentIntent(pendingIntentContent)
                .setSmallIcon(R.drawable.ic_notify)
                .setLargeIcon(bitmapLargeIcon)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(description));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFY_ID, NOTIFY_CHANNEL,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify((int) (Math.random() * 5000 + 1), builder.build());
        }
    }
}
