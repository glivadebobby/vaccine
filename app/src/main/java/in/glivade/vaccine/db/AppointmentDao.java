package in.glivade.vaccine.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.glivade.vaccine.model.Appointment;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface AppointmentDao {
    @Query("SELECT * FROM appointment")
    List<Appointment> get();

    @Query("SELECT * FROM appointment")
    LiveData<List<Appointment>> getAll();

    @Query("DELETE FROM appointment")
    void nuke();

    @Insert(onConflict = REPLACE)
    void insertAll(Appointment... appointments);

    @Delete
    void deleteAll(Appointment... appointments);
}
