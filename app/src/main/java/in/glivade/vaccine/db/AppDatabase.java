package in.glivade.vaccine.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import in.glivade.vaccine.model.Appointment;
import in.glivade.vaccine.model.Children;
import in.glivade.vaccine.model.Vaccine;

@Database(entities = {Vaccine.class, Children.class, Appointment.class},
        version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "vaccine";
    private static AppDatabase sInstance;

    public static AppDatabase getAppDatabase(Context context) {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return sInstance;
    }

    public abstract VaccineDao vaccineDao();

    public abstract ChildrenDao childrenDao();

    public abstract AppointmentDao appointmentDao();
}
