package in.glivade.vaccine.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.glivade.vaccine.model.Children;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ChildrenDao {
    @Query("SELECT * FROM children")
    List<Children> get();

    @Query("SELECT * FROM children")
    LiveData<List<Children>> getAll();

    @Query("DELETE FROM children")
    void nuke();

    @Insert(onConflict = REPLACE)
    void insertAll(Children... children);

    @Delete
    void deleteAll(Children... children);
}
