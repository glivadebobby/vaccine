package in.glivade.vaccine.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.glivade.vaccine.model.Vaccine;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface VaccineDao {
    @Query("SELECT * FROM vaccine")
    List<Vaccine> get();

    @Query("SELECT * FROM vaccine")
    LiveData<List<Vaccine>> getAll();

    @Query("DELETE FROM vaccine")
    void nuke();

    @Insert(onConflict = REPLACE)
    void insertAll(Vaccine... vaccines);
}
