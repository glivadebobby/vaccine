package in.glivade.vaccine;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.glivade.vaccine.adapter.FeedAdapter;
import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.app.MyPreference;
import in.glivade.vaccine.model.Feed;
import in.glivade.vaccine.util.NotificationHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static in.glivade.vaccine.app.Api.KEY_CREATED_AT;
import static in.glivade.vaccine.app.Api.KEY_DESC;
import static in.glivade.vaccine.app.Api.KEY_FCM_TOKEN;
import static in.glivade.vaccine.app.Api.KEY_ID;
import static in.glivade.vaccine.app.Api.KEY_IMAGE;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.KEY_TITLE;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_FEEDS;
import static in.glivade.vaccine.app.Api.VALUE_UPDATE_FCM;
import static in.glivade.vaccine.app.MyActivity.launch;
import static in.glivade.vaccine.app.MyActivity.launchClearStack;
import static in.glivade.vaccine.db.AppDatabase.getAppDatabase;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        NavigationView.OnNavigationItemSelectedListener {

    private Context context;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewFeeds;
    private NavigationView navigationView;
    private List<Feed> feedList;
    private FeedAdapter feedAdapter;
    private AnimationDrawable mAnimationDrawable;
    private ActionBarDrawerToggle drawerToggle;
    private FusedLocationProviderClient providerClient;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initToolbar();
        initRecyclerView();
        initRefresh();
        initDrawer();
        initLocation();
        updateFcm();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(Gravity.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 4:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED)
                    initLocation();
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onRefresh() {
        getFeeds();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawers();
        if (item.getItemId() == R.id.nav_children) {
            launch(this, ChildrenActivity.class);
        } else if (item.getItemId() == R.id.nav_vaccines) {
            launch(this, VaccineActivity.class);
        } else if (item.getItemId() == R.id.nav_appointments) {
            launch(this, AppointmentActivity.class);
        } else if (item.getItemId() == R.id.nav_setting) {
            new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    preference.setHour(hourOfDay);
                    preference.setMinute(minute);
                    NotificationHelper.cancelAlarm();
                    NotificationHelper.scheduleRepeatingNotification(MainActivity.this);
                    Toast.makeText(MainActivity.this, "Notification time updated", Toast.LENGTH_SHORT).show();
                }
            }, preference.getHour(), preference.getMinute(), false).show();
        } else if (item.getItemId() == R.id.nav_logout) {
            getAppDatabase(this).vaccineDao().nuke();
            getAppDatabase(this).childrenDao().nuke();
            getAppDatabase(this).appointmentDao().nuke();
            preference.clearUser();
            launchClearStack(this, SplashActivity.class);
        }
        return false;
    }

    private void initObjects() {
        drawerLayout = findViewById(R.id.drawer);
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewFeeds = findViewById(R.id.feeds);
        navigationView = findViewById(R.id.nav);

        context = this;
        feedList = new ArrayList<>();
        feedAdapter = new FeedAdapter(feedList);
        mAnimationDrawable = (AnimationDrawable) drawerLayout.getBackground();
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        providerClient = LocationServices.getFusedLocationProviderClient(this);
        preference = new MyPreference(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initRecyclerView() {
        viewFeeds.setLayoutManager(new LinearLayoutManager(context));
        viewFeeds.setAdapter(feedAdapter);
    }

    private void initRefresh() {
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                getFeeds();
            }
        });
    }

    private void initDrawer() {
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initLocation() {
        if (hasLocationPermission()) {
            providerClient.getLastLocation().addOnSuccessListener(this,
                    new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                preference.setLatitude(location.getLatitude());
                                preference.setLongitude(location.getLongitude());
                            }
                        }
                    });
        } else requestLocationPermission();
    }

    private void getFeeds() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        refreshLayout.setRefreshing(false);
                        handleFeedsResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                Toast.makeText(MainActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_FEEDS);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "feeds");
    }

    private void handleFeedsResult(String response) {
        try {
            feedList.clear();
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String title = jsonObject.getString(KEY_TITLE);
                String description = jsonObject.getString(KEY_DESC);
                String date = jsonObject.getString(KEY_CREATED_AT);
                String image = jsonObject.getString(KEY_IMAGE);
                feedList.add(new Feed(title, description, date, image));
            }
            feedAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateFcm() {
        if (preference.isTokenUploaded() || preference.getFcmToken() == null || preference.getId() == -1)
            return;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        preference.setTokenUploaded(true);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_UPDATE_FCM);
                params.put(KEY_ID, String.valueOf(preference.getId()));
                params.put(KEY_FCM_TOKEN, preference.getFcmToken());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "update_fcm");
    }

    private boolean hasLocationPermission() {
        return ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED;
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 4);
    }
}
