package in.glivade.vaccine;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.glivade.vaccine.adapter.VaccineAdapter;
import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.db.AppDatabase;
import in.glivade.vaccine.model.Vaccine;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.vaccine.app.Api.KEY_DOSE1;
import static in.glivade.vaccine.app.Api.KEY_DOSE2;
import static in.glivade.vaccine.app.Api.KEY_DOSE3;
import static in.glivade.vaccine.app.Api.KEY_DOSE4;
import static in.glivade.vaccine.app.Api.KEY_DOSE5;
import static in.glivade.vaccine.app.Api.KEY_ID;
import static in.glivade.vaccine.app.Api.KEY_NAME;
import static in.glivade.vaccine.app.Api.KEY_PREVENTS;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_VACCINES;
import static in.glivade.vaccine.db.AppDatabase.getAppDatabase;

public class VaccineActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewVaccines;
    private List<Vaccine> vaccineList;
    private VaccineAdapter vaccineAdapter;
    private AnimationDrawable mAnimationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccine);
        initObjects();
        initToolbar();
        initRecyclerView();
        initRefresh();
        loadVaccines();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        getVaccines();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewVaccines = findViewById(R.id.vaccines);

        context = this;
        vaccineList = new ArrayList<>();
        vaccineAdapter = new VaccineAdapter(vaccineList);
        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.vaccine).getBackground();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewVaccines.setLayoutManager(new LinearLayoutManager(context));
        viewVaccines.setAdapter(vaccineAdapter);
    }

    private void initRefresh() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void loadVaccines() {
        LiveData<List<Vaccine>> data = getAppDatabase(context).vaccineDao().getAll();
        data.observe(this, new Observer<List<Vaccine>>() {
            @Override
            public void onChanged(@Nullable List<Vaccine> vaccines) {
                if (vaccines == null || vaccines.isEmpty()) {
                    refreshLayout.setRefreshing(true);
                    getVaccines();
                } else {
                    vaccineList.clear();
                    vaccineList.addAll(vaccines);
                    vaccineAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void getVaccines() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        refreshLayout.setRefreshing(false);
                        handleVaccinesResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                Toast.makeText(VaccineActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_VACCINES);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "vaccines");
    }

    private void handleVaccinesResult(String response) {
        try {
            List<Vaccine> vaccines = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                int dose1 = jsonObject.getInt(KEY_DOSE1);
                int dose2 = jsonObject.getInt(KEY_DOSE2);
                int dose3 = jsonObject.getInt(KEY_DOSE3);
                int dose4 = jsonObject.getInt(KEY_DOSE4);
                int dose5 = jsonObject.getInt(KEY_DOSE5);
                String name = jsonObject.getString(KEY_NAME);
                String prevents = jsonObject.getString(KEY_PREVENTS);
                vaccines.add(new Vaccine(id, dose1, dose2, dose3, dose4, dose5, name, prevents));
            }
            new AddVaccineTask(getAppDatabase(this))
                    .execute(vaccines.toArray(new Vaccine[vaccines.size()]));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static class AddVaccineTask extends AsyncTask<Vaccine, Void, Void> {

        private AppDatabase database;

        AddVaccineTask(AppDatabase database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Vaccine... vaccines) {
            database.vaccineDao().nuke();
            database.vaccineDao().insertAll(vaccines);
            return null;
        }
    }
}
