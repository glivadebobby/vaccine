package in.glivade.vaccine;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.glivade.vaccine.adapter.ChildrenAdapter;
import in.glivade.vaccine.app.AppController;
import in.glivade.vaccine.app.MyPreference;
import in.glivade.vaccine.db.AppDatabase;
import in.glivade.vaccine.model.Children;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.glivade.vaccine.app.Api.KEY_CHILD_ID;
import static in.glivade.vaccine.app.Api.KEY_DOB;
import static in.glivade.vaccine.app.Api.KEY_ERROR;
import static in.glivade.vaccine.app.Api.KEY_GENDER;
import static in.glivade.vaccine.app.Api.KEY_ID;
import static in.glivade.vaccine.app.Api.KEY_MESSAGE;
import static in.glivade.vaccine.app.Api.KEY_NAME;
import static in.glivade.vaccine.app.Api.KEY_TAG;
import static in.glivade.vaccine.app.Api.KEY_USER_ID;
import static in.glivade.vaccine.app.Api.URL;
import static in.glivade.vaccine.app.Api.VALUE_CHILDREN;
import static in.glivade.vaccine.app.Api.VALUE_DELETE_CHILD;
import static in.glivade.vaccine.app.MyActivity.launchWithBundle;
import static in.glivade.vaccine.util.Parser.getNiceTime;

public class ChildrenActivity extends AppCompatActivity implements View.OnClickListener,
        ChildrenAdapter.ChildrenCallback, SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewChildren;
    private FloatingActionButton buttonAdd;
    private List<Children> childrenList;
    private ChildrenAdapter childrenAdapter;
    private AnimationDrawable animationDrawable;
    private MyPreference preference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_children);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
        loadChildren();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            animationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonAdd) {
            startActivity(new Intent(this, AddChildActivity.class));
        }
    }

    @Override
    public void onDeleteClick(int position) {
        promptDeleteDialog(position);
    }

    @Override
    public void onGetAppointmentClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_CHILD_ID, childrenList.get(position).getId());
        launchWithBundle(this, CenterActivity.class, bundle);
    }

    @Override
    public void onRefresh() {
        getChildren();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewChildren = findViewById(R.id.children_list);
        buttonAdd = findViewById(R.id.fab_add);

        context = this;
        childrenList = new ArrayList<>();
        childrenAdapter = new ChildrenAdapter(childrenList, this);
        animationDrawable = (AnimationDrawable) findViewById(R.id.children).getBackground();
        preference = new MyPreference(context);
        progressDialog = new ProgressDialog(context);
    }

    private void initCallbacks() {
        buttonAdd.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewChildren.setLayoutManager(new LinearLayoutManager(context));
        viewChildren.setAdapter(childrenAdapter);
    }

    private void initRefresh() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void loadChildren() {
        LiveData<List<Children>> data = AppDatabase.getAppDatabase(context).childrenDao().getAll();
        data.observe(this, new Observer<List<Children>>() {
            @Override
            public void onChanged(@Nullable List<Children> children) {
                if (children == null || children.isEmpty()) {
                    refreshLayout.setRefreshing(true);
                    getChildren();
                } else {
                    childrenList.clear();
                    childrenList.addAll(children);
                    childrenAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void getChildren() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        refreshLayout.setRefreshing(false);
                        handleChildrenResult(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                Toast.makeText(ChildrenActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_CHILDREN);
                params.put(KEY_USER_ID, String.valueOf(preference.getId()));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "children");
    }

    private void handleChildrenResult(String response) {
        try {
            List<Children> children = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt(KEY_ID);
                String name = jsonObject.getString(KEY_NAME);
                String gender = jsonObject.getString(KEY_GENDER);
                String dob = getNiceTime(jsonObject.getString(KEY_DOB));
                children.add(new Children(id, name, gender, dob));
            }
            new AddChildrenTask(AppDatabase.getAppDatabase(this))
                    .execute(children.toArray(new Children[children.size()]));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void promptDeleteDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete");
        builder.setMessage("Are you sure you want to delete?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteChild(position);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void deleteChild(final int position) {
        showProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleDeleteChildResponse(position, response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                Toast.makeText(ChildrenActivity.this,
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_DELETE_CHILD);
                params.put(KEY_ID, String.valueOf(childrenList.get(position).getId()));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "delete-child");
    }

    private void handleDeleteChildResponse(int position, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

            if (error == 0) {
                new DeleteChildTask(AppDatabase.getAppDatabase(this))
                        .execute(childrenList.get(position));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Removing child..");
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private static class AddChildrenTask extends AsyncTask<Children, Void, Void> {

        private AppDatabase database;

        AddChildrenTask(AppDatabase database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Children... children) {
            database.childrenDao().nuke();
            database.childrenDao().insertAll(children);
            return null;
        }
    }

    private static class DeleteChildTask extends AsyncTask<Children, Void, Void> {

        private AppDatabase database;

        DeleteChildTask(AppDatabase database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Children... children) {
            database.childrenDao().deleteAll(children);
            return null;
        }
    }
}
