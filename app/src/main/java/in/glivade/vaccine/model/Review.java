package in.glivade.vaccine.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Review implements Parcelable {

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    private int id, centerId, userId;
    private float rating;
    private String review, name, mobile, lastUpdated;

    public Review(int id, int centerId, int userId, float rating, String review, String name,
                  String mobile, String lastUpdated) {
        this.id = id;
        this.centerId = centerId;
        this.userId = userId;
        this.rating = rating;
        this.review = review;
        this.name = name;
        this.mobile = mobile;
        this.lastUpdated = lastUpdated;
    }

    protected Review(Parcel in) {
        id = in.readInt();
        centerId = in.readInt();
        userId = in.readInt();
        rating = in.readFloat();
        review = in.readString();
        name = in.readString();
        mobile = in.readString();
        lastUpdated = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCenterId() {
        return centerId;
    }

    public void setCenterId(int centerId) {
        this.centerId = centerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(centerId);
        dest.writeInt(userId);
        dest.writeFloat(rating);
        dest.writeString(review);
        dest.writeString(name);
        dest.writeString(mobile);
        dest.writeString(lastUpdated);
    }
}
