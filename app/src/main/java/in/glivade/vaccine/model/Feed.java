package in.glivade.vaccine.model;

import in.glivade.vaccine.app.Api;

import static in.glivade.vaccine.util.Parser.getRelativeTime;

public class Feed {

    private String title, desc, date, image;

    public Feed(String title, String desc, String date, String image) {
        this.title = title;
        this.desc = desc;
        this.date = date;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDate() {
        return "Posted " + getRelativeTime(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFeedImage() {
        return Api.BASE_URL + image;
    }
}
