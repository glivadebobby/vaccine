package in.glivade.vaccine.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Vaccine {

    @PrimaryKey
    private int id;
    private int dose1, dose2, dose3, dose4, dose5;
    private String name, prevents;

    public Vaccine(int id, int dose1, int dose2, int dose3, int dose4, int dose5, String name, String prevents) {
        this.id = id;
        this.dose1 = dose1;
        this.dose2 = dose2;
        this.dose3 = dose3;
        this.dose4 = dose4;
        this.dose5 = dose5;
        this.name = name;
        this.prevents = prevents;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDose1() {
        return dose1;
    }

    public void setDose1(int dose1) {
        this.dose1 = dose1;
    }

    public int getDose2() {
        return dose2;
    }

    public void setDose2(int dose2) {
        this.dose2 = dose2;
    }

    public int getDose3() {
        return dose3;
    }

    public void setDose3(int dose3) {
        this.dose3 = dose3;
    }

    public int getDose4() {
        return dose4;
    }

    public void setDose4(int dose4) {
        this.dose4 = dose4;
    }

    public int getDose5() {
        return dose5;
    }

    public void setDose5(int dose5) {
        this.dose5 = dose5;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrevents() {
        return prevents;
    }

    public void setPrevents(String prevents) {
        this.prevents = prevents;
    }
}
